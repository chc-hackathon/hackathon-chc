<?php
session_start();
require "assets/includes/pdo.php";
require "assets/php/fetch.php";
include_once("assets/php/notifications.php");
if (isset($_SESSION["logged_in"])) {
//if (true) {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="assets/includes/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/styles/main.css">
    <title>Villaticus</title>
</head>
<body>
  
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-6">
      <a href=""><img class="logo_image" src="assets/images/VT_logo_small_BLUE.svg"></a>
      </div>

      <div class="col-sm-6 col-6">
          <p>
            
              <?php echo $_SESSION["user_fullname"] ?> <a href="components/user.php"><img src="assets/images/user.png"><a>
           
            <a id="logout" href="assets/php/logout.php"><img src="assets/images/logout.svg" alt=""></a>
          </p>
      </div>
    </div>
  </div>
  <div class="name">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
          <h1>Banov</h1>
      </div>

      
    </div>
  </div>
</div>
</header>

<div class="container">
  <div class="row overview">
    <div class="col-sm-2 col-6">
    <img src="assets/images/temperature.png"><p><span class="bigger_number" id="weatherTemp">--</span>°C<p>
    </div>

    <div class="col-sm-2 col-6">
    <img src="assets/images/wind-2.png"><p><span class="bigger_number" id="weatherWindSpeed">--</span>m/s<p>
    </div>

    <div class="col-sm-2 col-6">
    <img src="assets/images/cloud.png"><p><span class="bigger_number" id="weatherClouds">--</span>%<p>
    </div>

    <div class="col-sm-2 col-6">
    <img src="assets/images/humidity.png"><p><span class="bigger_number" id="weatherHumidity">--</span>%<p>
    </div>

    <div class="col-sm-2 col-6">
    <img src="assets/images/sunrise.png"><p><span class="bigger_number" id="weatherSunrise">--:--</span><p>
    </div>

    <div class="col-sm-2 col-6">
    <img src="assets/images/sunset.png"><p><span class="bigger_number" id="weatherSunset">--:--</span><p>
    </div>
  </div>
</div>

<script src="assets/js/weather.js"></script>
<script>
getWeatherData("https://api.openweathermap.org/data/2.5/weather?lat=48.988002&lon=17.719967&lang=cz&units=metric&appid=2cfc8a0e9aaed59735be710714a17288");
</script>

<div class="news container">
  <div class="row">

    <div class="col-sm-6 col-12">
      <a href="">
      <div class="block box-shadow">
        <div class="image_block"><img src="assets/images/calendar.png"></div>
        <div class="block_text">
          <?php
            /*
            $fetch = new Fetch();
            $event = $fetch->getSoonestEvent();
            echo "<p><h3>". date("d.m", strtotime($event["event_time"]))."</h3></p>";
            echo "<p>". $event["event_title"]."</p>";
          */
          ?>
        </div>
      </a>
      </div>

      <div class="aplication">
        
      <a href="">
        <div class="app" id="messages">
          <img src="assets/images/email.png">
        </div>
      </a>

      <a href="components/events.php">
        <div class="app" id="events">
          <img src="assets/images/calendar.png">
        </div>
      </a>

      <a href="components/map.php">
        <div class="app" id="map">
          <img src="assets/images/location.png">
        </div>
      </a>

      <a href="components/survey.php">
        <div class="app" id="survey">
          <img src="assets/images/survey.png">
        </div>
      </a>

      <a href="components/crossroads.php">
        <div class="app" id="market">
          <img src="assets/images/shopping-bag.png">
        </div>
      </a>

      <a href="components/forms.php">
        <div class="app" id="request">
          <img src="assets/images/document.png">
        </div>
      </a>

      <a href="components/companies.php">
        <div class="app" id="factory">
          <img src="assets/images/factory.png">
        </div>
      </a>
      </div>
    </div>

    <div class="col-sm-6 col-12" id="notificationsDiv">
      <div class="block" >
        <div class="image_block"><img src="assets/images/email.png"></div>
        <div class="block_text">
          <p><h3>Notifications</h3></p>
          <p>Latest messages</p>
        </div>
      </div>
        <?php
      /*
        $not = new Notifications();
        $user = $fetch->getUserData($_SESSION["user_username"], "username");
        $nots = $not->fetchNotifications($user["user_id"]);
        if (sizeof($nots) > 0) {
          foreach ($nots as $item) {
            ?>
            <div class="notification">
              <p><?php echo $item["not_title"]; ?></p><span class="notificationDelete" data-not-id="<?php echo $item["not_id"]; ?>">X</span>
            </div>
            <?php
          }
        }else{
          echo "<p class='notificationAlert'>There are no notifications in the moment.</p>";
        }
      */
        ?>
    </div>
    <script>
        $(".notificationDelete").on("click", function(){
          console.log($("#notificationsDiv").children().length);
          let notIdTemp = $(this).attr("data-not-id");
          $(this).parent().remove();
          $.ajax({
            type: "POST",
            url: "assets/php/notificationDelete.php",
            data: {notId: notIdTemp },
            success: function(result){
              console.log($("#notificationsDiv").children().length);
              if($("#notificationsDiv").children().length <= 1){
                $("#notificationsDiv").append("<p class='notificationAlert'>There are no notifications in the moment.</p>");
              }

            }
          });
          
          
        });
      </script>
    </div>    
</div>

      <div class="topuser container">

      <h1>Top users</h1>

        <table class="box-shadow">
        <?php
        $scoreTable = $fetch–>getPointTableByBest();

        foreach ($scoreTable as $user) {
          
        ?>
          <tr>
            <td>
              <h3><?php echo $user["user_id"] ?>.</h3>
            </td>

            <td>
              <p><?php echo $user["user_points"] ?>b.</p> 
            </td>

            <td>
              <p><?php echo $user["user_firstname"]." ".$user["user_lastname"] ?> ?></p> 
            </td>
          </tr>
        <?php }?>

          <tr>
            <td>
              <h3>2.</h3>
            </td>

            <td>
              <p>50b.</p> 
            </td>

            <td>
              <p>Marie Fs</p> 
            </td>
          </tr>

          <tr>
            <td>
              <h3>3.</h3>
            </td>

            <td>
              <p>20b.</p> 
            </td>

            <td>
              <p>Marie Fs</p> 
            </td>
          </tr>

        </table>

      </div>

</body>
</html>

<?php
}else {
  header("Location:index.php");
}


?>