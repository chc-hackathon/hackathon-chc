<?php 
    session_start();
    require "assets/includes/pdo.php";
    require "assets/php/register.php";
    require "assets/php/fetch.php";
    
    $fetch = new Fetch();

    $reg = new Register();
    $codeCheck = false;
    $fname = '';
    $lname = '';

    if(isset($_POST["submit"])){
        $code = $_POST["user_code"];
        $codeCheck = $reg->codeCheck($code);
        $userData = $fetch->getUserData($code,"code");
        $fname = $userData["user_firstname"];
        $lname = $userData["user_lastname"];
        $username = $userData["user_username"];
        $_SESSION["code"] = $code;
    }if(isset($_POST["reg_confirm"])){
        $pwd1 = $_POST["user_password1"];
        $pwd2 = $_POST["user_password2"];
        $email = $_POST["user_email"];
        $reg->registerFunc($pwd1,$pwd2,$email);
        header("Location:index.php");
    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="assets/styles/login.css">
    <link rel="stylesheet" type="text/css" href="media_queries.css">
    <title></title>
</head>
<body>
<div class="wrapper">

<div class="div1">

   <div class="div1_in1">

        <div class="login">

        
    <?php if($codeCheck == true){ //Pokud je zadaný kód, vypíše se zadávání emailu atd ?> 
        <h2 class="h2_reg2">Making your account</h2> 
        <form action="register.php" method="POST">
            <p>Firstname<br><b><?php echo $fname;?></b></p>
            <p>Lastname<br><b><?php echo $lname;?></b></p>
            <p>Username<br><b><?php echo $username;?></b></p><br>
            <input type="email" name="user_email"  placeholder="E-mail" required><br>
            <input type="password" name="user_password1" placeholder="Password" required><br>
            <input type="password" name="user_password2" placeholder="Confirm password" required><br>
            <button type="submit" value="CONFIRM" name="reg_confirm">CONFIRM</button>
        </form>
    <?php } else { //Zadávání kódu?>
        <h2 class="h2_reg">First time<br>here?</h2>
        <form action="register.php" method="POST">
            <input type="number" name="user_code" placeholder="activation key" required>
            <br>
            <button type="submit" name ="submit">Next step</button>
        </form>

    <?php } ?>

    </div>
            </div>

        </div>

        <div class="div2">
            
        </div>

    </div>
</body>
</html>