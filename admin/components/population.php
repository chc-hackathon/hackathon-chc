<?php
        require "admin_func.php";
        $func = new Func();
    if(isset($_POST["submit"])){
        $fname = preg_replace('/[0-9]+/', '', $_POST["user_firstname"]);
        $lname = preg_replace('/[0-9]+/', '', $_POST["user_lastname"]);

        $code = sprintf("%06d", mt_rand(1, 999999));
        $codeCheck = $func->control_codes($code);
                while ($codeCheck > 0) {
                    $code = sprintf("%06d", mt_rand(1, 999999));
                    $codeCheck = $func->control_codes($code);
                }

        $username = $func->remove_accents($fname.$lname);
        $username = strtolower($username);
        $usernameCheck = $func->usernameCheck($username); 
            $i = 0;
            while ($usernameCheck > 0) {
                $i++;
                $username = $username . $i;
                $usernameCheck = $func->usernameCheck($username);
            }
        $func->newPerson($fname,$lname,$code,$username);            
    }
?>


<div class="container population">
    <h2>Create resident</h2>
    <div class="user-add">
        <form action="" method="post">
            <input type="text" name="user_firstname" placeholder="Firstname" required>
            <input type="text" name="user_lastname" placeholder="Lastname" required>
            <button type="submit" value="" name="submit">Add</button>
        </form>
    </div>
    <h2>Resident list</h2>
    <table>
    <?php 
        $array = $func->getPopulation();

        foreach ($array as $ar) {
            echo "<tr><td>"
            .$ar["user_id"]."</td><td>"
            .$ar["user_firstname"]."</td><td>"
            .$ar["user_lastname"]."</td><td>"
            .$ar["user_username"]."</td><td>"
            .$ar["user_email"]."</td><td>"
            .$ar["user_code"]."</td><td>"
            ."<button value=".$ar["user_id"].">EDIT</button></td>"
            ."<td><button value=".$ar["user_id"].">DELETE</button></td></tr>";
        }
    
    ?>
    </table>
</div>