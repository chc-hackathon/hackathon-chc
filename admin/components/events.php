<?php
    require "admin_func.php";
    $func = new Func();
    if(isset($_POST["submit"])){
        if (isset($_POST["event_title"], $_POST["event_desc"], $_POST["event_date"])) {
            $eventTitle = $_POST["event_title"];
            $eventDesc = $_POST["event_desc"];
            $eventDate = $_POST["event_date"];

            $func->createEvent($eventTitle,$eventDesc, $eventDate);
            header("Location:index.php");
        }           
    }
?>
<div class="container">
    <div class="event-add">
        <h2>Create event</h2><br>
        <form action="" method="post">
            
            <input type="text" name="event_title" placeholder="Title" required>

            <input type="text" name="event_desc" placeholder="Description" required>
     
            <input type="date" name="event_date" placeholder="Date" required>

            <button type="submit" value="" name="submit">Add</button>
        </form>
    </div>
</div>