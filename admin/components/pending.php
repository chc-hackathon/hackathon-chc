<?php
    require "admin_func.php";
    $func = new Func();
    $comp =$func->getPending("comp","companies");

    if(isset($_POST["accept"])){
        $func->acceptDecline("companies","comp","1",$_POST["accept"]);
        header("Refresh:0");
    }else if(isset($_POST["decline"])){
        $func->decline($_POST["decline"]);
        header("Refresh:0");
    }

?> 
<div class="container">
    <h2>Companies</h2>
    <?php
         $path = "../assets/images/company/";
        foreach ($comp as $c) {
            $file = $c["comp_img"];
            echo "<div class='row'><div class='col'>";
            echo  '<img src="'.$path.$file.'">';
            echo "<h2>".$c["comp_name"]."</h2>";
            echo "<p>".$c["comp_adress"]."</p>";
            echo "<p>".$c["comp_url"]."</p>";
            echo "<p>".$c["comp_open"]."-".$c["comp_closed"]."</p>";
            echo "<p>".$c["comp_desc"]."</p>";
            echo "<form action='' method='post'><button name='accept' value='".$c["comp_id"]."'>ACCEPT</button><button name='decline' value='".$c["comp_id"]."'>DECLINE</button></form>";
            echo "</div></div>";
        }
        
        ?>
</div>  
