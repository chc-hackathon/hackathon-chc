<?php

include_once("../assets/includes/pdo.php");
Db::connect("localhost", "mojededinacz1", "root", "");
//Db::connect("127.0.0.1", "mojededinacz1", "mojededina.cz", "cdfSXFqt8ioX");

session_start();
if(isset($_SESSION["admin_logged_in"])){

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../assets/styles/main.css">
    <link rel="stylesheet" href="../assets/styles/admin.css">
    <title>ADMINISTRATION</title>
</head>
<body>
    <div class="wrapper">
        <div class="admin">

        <nav>
            <?php
            if($_GET){
                ?>
                <span><a href="index.php"><-</a></span>
                <?php
            }
            ?>

<div class="container">
    <div class="row">
      <div class="col-sm-6 col-6">
      <a href=""><img class="logo_image" src="../assets/images/VT_logo_small_BLUE.svg"></a>
      </div>

      <div class="col-sm-6 col-6">
          <p>
            ADMINISTRATION
            <a id="logout" href="admin_logout.php"><img src="../assets/images/logout.svg" alt=""></a>
          </p>
      </div>
    </div>
  </div>


        </header>

        <div class="wrapper">
        <div class="admin">

        <?php if(!$_GET){?>
        <div class="container-admin">
            <a href="index.php?population" class="boxes box-shadow">
                <p>POPULATION</br>MANAGEMENT</p>
            </a>
            <a href="index.php?events" class="boxes box-shadow">
                <p>EVENTS</p>
            </a>
            <a href="index.php?polls" class="boxes box-shadow">
                <p>POLLS</p>
            </a>
            <a href="index.php?pending"class="boxes">
                <p>PENDING</p>
            </a>
        </div>

        <?php }
        else if(isset($_GET["population"])){
            include "components/population.php";
        }else if(isset($_GET["events"])){
            include "components/events.php";
        }else if(isset($_GET["polls"])){
            include "components/polls.php";
        }else if(isset($_GET["pending"])){
            include "components/pending.php";
        }
        ?>

        </div>
    </div>
</body>
</html>

<?php
} else{
    //zobrazit admin login page

    if (isset($_POST["admin_login_submit"])) {
        //Pokud někdo poslal login form
        //ověření vstupních inormací
        if(isset($_POST["admin_username"], $_POST["admin_pwd"])){
            $admin_username = $_POST["admin_username"];
            $admin_pwd = $_POST["admin_pwd"];

            if(!empty($admin_username) && !empty($admin_pwd)){
                $rowCount = Db::query("SELECT admin_username, admin_password FROM admins WHERE admin_username = ?", $admin_username);

                if ($rowCount > 0) {
                    $adminDb = Db::queryOne("SELECT admin_username, admin_password FROM admins WHERE admin_username = ?", $admin_username);
                    $pwdCheck = password_verify($admin_pwd, $adminDb["admin_password"]);
                    if ($pwdCheck) {
                        $_SESSION["admin_logged_in"] = true;
                        header("Refresh:0");
                        exit();
                    }else{
                        //error - špatné heslo
                        echo "Špatné heslo";
                    }
                } else{
                    //error - uživatel neexistuje
                    echo "uživatel neex";
                }

            }else{
                //error - prázdné pole
                echo "empty";
            }
        }
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../assets/styles/login.css">
    <link rel="stylesheet" type="text/css" href="../assets/styles/media_queries.css">
    <link rel="shortcut icon" href="../assets/images/VT_logo_small_white.svg" type="image/x-icon">
    <title>Villaticus</title>
</head>

<body>

    <div class="wrapper">

        <div class="div1">

           <div class="div1_in1">

                <div class="logo">
                    <img src="../assets/images/VT_logo_white.svg" alt="logo">
                    <p>Information system for village citizens - <b>simply</b>.</p>                                                           <!-- logo  JE TŘEBA VLOŽIT!!!!!-->
                </div>

                <div class="login">

                    <h2>Log in as admin</h2>

                    <form action="index.php" method="post">
                        <input type="text" name="admin_username" placeholder="Username" required>
                        <br>
                        <input type="password" name="admin_pwd" placeholder="Password" required>
                        <br>
                        <button type="submit" name="admin_login_submit">Log In</button>
                    </form>

                    <!-- WOF -->

                </div>

            </div>

        </div>

        <div class="div2" style="filter: saturate(0)">

        </div>

    </div>

</body>

</html>
<?php
}
?>
