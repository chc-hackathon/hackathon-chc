<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../assets/styles/main.css">
    <link rel="stylesheet" type="text/css" href="media_queries.css">
    <title><?php echo $title;?></title>
</head>
<body>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-6">
      <a href="../home.php"><img class="logo_image" src="../assets/images/VT_logo_small_BLUE.svg"></a>
      </div>

      <div class="col-sm-6 col-6">
          <p>
            
            <?php echo $_SESSION["user_fullname"] ?> <a href="user.php"><img src="../assets/images/user.png"><a>
           
            <a id="logout" href="../assets/php/logout.php"><img src="../assets/images/logout.svg" alt=""></a>
          </p>
      </div>
    </div>
  </div>
  <div class="name">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 flex">
          <?php if($title == "User"){echo"<h1>". $_SESSION["user_fullname"] . "</h1><span class='points'>".$_SESSION["points"]."p</span>";}else{echo "<h1>Banov</h1>";}?>
      </div>
    </div>
  </div>
</div>
</header>