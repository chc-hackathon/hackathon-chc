<?php 
  require "../assets/includes/pdo.php";
  require "../assets/php/fetch.php";
  $fetch = new Fetch();
  $title = "Forms";

  include "header.php"; 
?>
<?php if(!$_GET){?>
<div class="container">
    <div class="row">
        <div class="col-lg-4 form-col"><a href="?comp-reg">COMPANY REGISTRATION</a></div>
        <div class="col-lg-4 form-col"><a href="?fireworks">FIREWORK ALLOWENCE</a></div>
        <div class="col-lg-4 form-col"><a href="?space">LEASE SPACE</a></div>
    </div>
</div>
<?php }
        else if(isset($_GET["comp-reg"])){
            include "forms/company-reg.php";
        }else if(isset($_GET["fireworks"])){
            include "forms/fireworks.php";
        }else if(isset($_GET["space"])){
            include "forms/space.php";
        }
 ?>
