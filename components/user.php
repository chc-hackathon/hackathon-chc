<?php 
  require "../assets/includes/pdo.php";
  require "../assets/php/fetch.php";
  $fetch = new Fetch();
  $title = "User"; 
  include "header.php";

  $data = $fetch->getUserData($_SESSION["user_username"],"username");

  if(isset($_POST["change"])){
    
  }

?>
<div class="container user">
    <div class="row">
        <div class="col-lg-6">
            <h2>Basic informations</h2>
            <div class="row">
                <div class="col-lg-12 data"><p class="title">Username</p><input type="text changable" class="text" value="<?php echo $data["user_username"];?>" readonly></input></div>
                <div class="col-lg-12 data"><p class="title">Email</p><p class="text"><?php echo $data["user_email"]; ?></p></div><br><br>
            </div>
            <h2>Additional informations</h2>
            <div class="row">
                <div class="col-lg-12 data"><p class="title">Age</p><input type="number" class="text" id="chg" value="<?php echo "0"; ?>" readonly></input></div>
                <div class=" col-lg-12 data"><p class="title">Tel.Number</p><input type="number" class="text" id="chg" value="<?php echo 608670446; ?>" readonly></input></div>
            </div>
        </div>
        <div class="col-lg-6 picture">
            <img class="profile_pic" src="../assets/images/user.png" alt="">
        </div>
    </div>
    <button value="CHANGE" class="change" name="change">CHANGE</button>
</div>
    <script>
        var change = false;
        $(".change").click(function(){
                if(change == false){
                    $('#chg').removeAttr('readonly');
                    change = true;
                    
                }else{
                    $('#chg').prop('readonly', true);
                    change = false;
                }
        });
    </script>
</body>
</html>