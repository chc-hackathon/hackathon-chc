


<?php
    if(isset($_POST["submit"])&& isset($_FILES["comp_thumbnail"])){
        $name = $_POST["comp_name"];
        $desc = $_POST["comp_desc"];
        $adress = $_POST["comp_adress"];
        $website = $_POST["comp_website"];
        $ohours = $_POST["comp_o_hours"];
        $chours = $_POST["comp_c_hours"];

        $file = $_FILES["comp_thumbnail"];
        $fileName = $_FILES["comp_thumbnail"]["name"];
        $fileTmpName = $_FILES["comp_thumbnail"]["tmp_name"];
        $fileSize = $_FILES["comp_thumbnail"]["size"];
        $fileError = $_FILES["comp_thumbnail"]["error"];
        $fileType = $_FILES["comp_thumbnail"]["type"];

        $fileExt = explode(".",$fileName);
        $fileAct = strtolower(end($fileExt));

        $allowed = array("jpg","jpeg","png");
        
        if(in_array($fileAct,$allowed)){
            if($fileError === 0){
                    $fileNameNew = $name.".".$fileAct;
                    $fileDestination = "../assets/images/company/" . $fileNameNew;
                    move_uploaded_file($fileTmpName,$fileDestination);
                    $fetch = new Fetch();
                    $fetch->companyInsert($name,$desc,$adress,$website,$ohours,$chours,$fileNameNew);
            }
        }
    }
?>

<script src="../../assets/includes/jquery.js"></script>

<div class="container forms-style">
    <div class="row">
        <div class="col-sm-6">
            <form action="" method="post" enctype="multipart/form-data">
            <p>Name:</p>
            <input type="text" name="comp_name" id="" required>
            <p>Description:</p>
            <textarea name="comp_desc" id="textarea" cols="30" rows="10" required></textarea>
            <p id="letters_left">Remaining letters : 255</p>
            </div>
        <div class="col-sm-6">
            <p>Adress:</p>
            <input type="text" name="comp_adress" id="" required>
            <p>Website:</p>
            <input type="text" name="comp_website" id="" required>
            <p>Opening hours:</p>
            <input class="time" type="time" name="comp_o_hours" >
            <p>Closing hours:</p>
            <input class="time" type="time" name="comp_c_hours" ><br>
            <input type="file" name="comp_thumbnail" value="Vybrat soubor"></br>
            <input type="submit" name="submit" value="SUBMIT">
            </form> 
        </div>
    </div>
</div>

<script>
    $("#textarea").keydown(function(e){

            if(this.value.length > 255 && e.keyCode !== 8){
                return false;
            }
    $("#letters_left").html("Remaining letters : " +(255 - this.value.length));
    })
</script>