-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2019 at 08:31 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mojededinacz1`
--

-- --------------------------------------------------------

--
-- Table structure for table `additionaldata`
--

CREATE TABLE `additionaldata` (
  `user_id` int(6) NOT NULL,
  `user_points` int(10) DEFAULT NULL,
  `user_birthday` date DEFAULT NULL,
  `user_telnumber` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `admin_username` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `admin_password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `admin_email` varchar(50) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_username`, `admin_password`, `admin_email`) VALUES
(000001, 'admin', '$2y$10$1YljDbzN.i1TObIXYPNS3uQbAkJPtj9huLCIERw5smGXPyX146zwa', 'admin@email.cz');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `comp_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `comp_name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `comp_desc` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `comp_adress` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `comp_url` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `comp_open` varchar(6) COLLATE utf8_czech_ci NOT NULL DEFAULT '00:00',
  `comp_closed` varchar(6) COLLATE utf8_czech_ci NOT NULL DEFAULT '00:00',
  `comp_img` varchar(125) COLLATE utf8_czech_ci NOT NULL,
  `comp_status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`comp_id`, `comp_name`, `comp_desc`, `comp_adress`, `comp_url`, `comp_open`, `comp_closed`, `comp_img`, `comp_status`) VALUES
(000001, 'Penny', 'Obchůdek s jídlem', 'Rovná 153', '', '7:00', '15:00', '', 1),
(000002, 'Bistro U Tonyho', 'Chinnese bistro', 'Rovná 154', '', '9:00', '18:00', '', 1),
(000004, 'awdawd', 'awdawdawd', 'awdawd', 'awdawd', '03:00', '02:58', 'awdawd.jpg', 1),
(000005, 'awdawd', 'awdawdawd', 'awdawd', 'awdawd', '03:00', '02:58', 'awdawd.jpg', 1),
(000006, 'awdawd', 'awdawdawd', 'adwdawdawd', 'awdawdawd', '14:01', '14:01', 'awdawd.jpg', 1),
(000007, 'awdawd', 'awdawdawd', 'adwdawdawd', 'awdawdawd', '14:01', '14:01', 'awdawd.jpg', 1),
(000008, 'awdawd', 'awdawdawd', 'adwdawdawd', 'awdawdawd', '14:01', '14:01', 'awdawd.jpg', 1),
(000009, 'awdawd', 'awdawdawd', 'awdawd', 'wdawd', '14:01', '04:01', 'awdawd.jpg', 1),
(000010, 'awdawd', 'awdawdawd', 'awdawd', 'wdawd', '14:01', '04:01', 'awdawd.jpg', 1),
(000011, 'awdawd', 'awdawdawdawd', 'wwwww', 'wwwww', '14:01', '14:01', 'awdawd.jpg', 1),
(000012, 'awdawd', 'awdawdawdawd', 'wwwww', 'wwwww', '14:01', '14:01', 'awdawd.jpg', 1),
(000013, 'awdawd', 'awdawdawdawd', 'wwwww', 'wwwww', '14:01', '14:01', 'awdawd.jpg', 1),
(000014, 'Stuchlik a.s.', 'neco bo adam', 'Zlin 123', 'adam.com', '10:00', '14:05', 'Stuchlik a.s..jpg', 0),
(000015, 'Nosek s.r.o.', 'bo Zorka', 'Kleš 12', 'kesi.nej', '02:20', '17:30', 'Nosek s.r.o..jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `event_title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `event_desc` text COLLATE utf8_czech_ci NOT NULL,
  `event_time` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `event_title`, `event_desc`, `event_time`) VALUES
(000001, 'Bánovské hody', 'ADwadawdawdawdawdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', '2019-11-29'),
(000002, 'Test', 'test test', '2019-12-28');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `not_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `not_title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `not_href` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `not_user_id` int(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Table structure for table `poll`
--

CREATE TABLE `poll` (
  `poll_id` int(6) NOT NULL,
  `poll_que` varchar(150) NOT NULL,
  `poll_a1` varchar(150) DEFAULT NULL,
  `poll_a2` varchar(150) DEFAULT NULL,
  `poll_a3` varchar(150) DEFAULT NULL,
  `poll_a4` varchar(150) DEFAULT NULL,
  `poll_r1` int(10) DEFAULT NULL,
  `poll_r2` tinyint(1) DEFAULT NULL,
  `poll_r3` tinyint(1) DEFAULT NULL,
  `poll_r4` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pollanswer`
--

CREATE TABLE `pollanswer` (
  `user_id` int(6) NOT NULL,
  `poll_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(6) UNSIGNED ZEROFILL NOT NULL,
  `user_firstname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `user_lastname` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `user_username` varchar(60) COLLATE utf8_czech_ci NOT NULL,
  `user_email` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `user_password` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `user_code` int(6) UNSIGNED NOT NULL,
  `user_points` int(6) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_firstname`, `user_lastname`, `user_username`, `user_email`, `user_password`, `user_code`, `user_points`) VALUES
(000002, 'Adam', 'Smetana', 'adamsmetana', 'nevim@nevim.cz', '132456', 123456, 0),
(000003, 'David', 'Marek', 'davidmarek', 'david@kokot.cz', '$2y$10$7YPMLXaTLn8YhqbZ5HGqOuBu33.qjhuLhyu6X1Moet3dGiZk3PopW', 555555, 0),
(000004, 'Petr', 'Nosek', 'petrnosek', '', '', 973085, 0),
(000007, 'Jaromír', 'Stuchlík', 'jaromirstuchlik', 'jarek.stuchlik@gmail.com', '$2y$10$rcqlhUs0CGWdlNKie0ZIjuoAQdAHup1vmXLUiNS9G0ly4pVkHxjci', 546060, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additionaldata`
--
ALTER TABLE `additionaldata`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`not_id`);

--
-- Indexes for table `poll`
--
ALTER TABLE `poll`
  ADD PRIMARY KEY (`poll_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `comp_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `not_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `poll`
--
ALTER TABLE `poll`
  MODIFY `poll_id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
