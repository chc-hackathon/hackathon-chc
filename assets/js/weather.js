function getWeatherData(api) {
  $.ajax({
    url: api,
    success: function(result) {
      writeWeatherData(result);
      console.table(result);
    }
  });
}

function timestampToTime(timestamp) {
  let d = new Date(timestamp * 1000);
  return (
    d.getHours() +
    ":" +
    (d.getMinutes() > 9 ? d.getMinutes() : "0" + d.getMinutes())
  );
}

function writeWeatherData(weatherData) {
  $("#weatherTemp").html(weatherData.main.temp);
  $("#weatherWindSpeed").html(weatherData.wind.speed);
  $("#weatherClouds").html(weatherData.clouds.all);
  $("#weatherHumidity").html(weatherData.main.humidity);
  $("#weatherSunrise").html(timestampToTime(weatherData.sys.sunrise));
  $("#weatherSunset").html(timestampToTime(weatherData.sys.sunset));
}
