<?php
    class Register{
        public function codeCheck($code){
            global $fetch;
            $result = $fetch->control_codes($code); 
            if($result == 0){
                return false;
            }else{
                return true;
            }
        }

        public function registerFunc($pwd1,$pwd2,$email){
            global $fetch;
            $em = strip_tags($email);
            $em= str_replace("","",$em);

            $pw1 = strip_tags($pwd1);
            $pw2 = strip_tags($pwd2);

            if(filter_var($em,FILTER_VALIDATE_EMAIL)){
                $em = filter_var($em, FILTER_VALIDATE_EMAIL);
                $emCheck = $fetch->emailCheck($em);
                if($emCheck > 0){
                    // Error na to, že je email již použit
                }
            }else{
                // Nesprávný formát
            }

            if($pw1 !== $pw2){
                echo "Hesla se neshodují";
            }else{
                if (preg_match("/[^A-Za-z0-9]/", $pw1)) {
                    echo "Nesprávné znaky";
                }
            }

            if(strlen($pw1) > 30 || strlen($pw1) < 5) {
               echo "Heslo je moc dlouhé, musí být mezi 5 a 30 znaky!";
            }

            //if(empty($errorArray)){}

            $pw_hashed = password_hash($pw1,PASSWORD_DEFAULT);
            

            $insert = $fetch->registerInsert($pw_hashed,$email,$_SESSION["code"]);

        }
    }
?>