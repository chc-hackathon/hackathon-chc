<?php
//require_once("assets/includes/pdo.php");
//Db::connect("localhost", "mojededinacz1", "root","");
//Db::connect("127.0.0.1", "mojededinacz1", "mojededina.cz", "cdfSXFqt8ioX");

class Notifications{
    function notifyOne($userId, $notTitle){
        Db::insert("notifications", array("", $notTitle, "", $userId));
    }
    function notifyOneHref($userId, $notTitle, $notHref){
        Db::insert("notifications", array("", $notTitle, $notHref, $userId));
    }
    function notifyAll($notTitle){
        Db::insert("notifications", array("", $notTitle, "", 0));
    }
    function notifyAllHref($notTitle, $notHref){
        Db::insert("notifications", array("", $notTitle, $notHref, 0));
    }
    function deleteNotification($notId){
        Db::query("DELETE FROM notifications WHERE not_id = ?", $notId);
    }

    function haveNotifications($userId){
        $nots = Db::queryAll("SELECT * FROM notifications WHERE not_user_id = ? OR not_user_id = 0", $userId);
        return ($nots > 0 ? true : false);
    }

    function fetchNotifications($userId){
        $nots = Db::queryAll("SELECT * FROM notifications WHERE not_user_id = ? OR not_user_id = 0", $userId);
        return $nots;
    }

}