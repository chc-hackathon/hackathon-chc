@import "fonts";
$darkblue: #264f73;
$lightblue: #c8d8e5;
$whiteblue: #ecf2f7;
$coral: #ff7350;
$lightcoral: #ffe8e2;
$white: #eff4f5;
$grey: #999;
$f-darkgrey: #111111;
$f-lightgrey: #cccccc;

* {
  font-family: Montserrat;
  box-sizing: border-box;
}
input[type="text"],
input[type="number"],
input[type="date"] {
  background: $lightblue;
  border: none;
  outline: none;
  font-size: 14px;
  padding: 16px;
  margin: 0 4px 4px 0;
  width: auto;

  &::placeholder {
    color: $darkblue;
    opacity: 0.9;
    font-weight: 600;
  }
}

#back{
  img{
    width: 40px;
    position: absolute;
    margin-left:10px;
    margin-top:10px;
  }
}

button {
  background: $lightcoral;
  color: $coral;
  outline: none;
  border: none;
  font-weight: 600;
  font-size: 14px;
  padding: 16px 32px;
  margin: 0 4px 4px 0;
}
a {
  color: $darkblue;
  &:visited {
    color: $darkblue;
  }

  &:hover {
    text-decoration: underline;
  }
}
.admin {
  height: 100vh;

  .login{
    
    input, select, textarea{
      width: 75%;
      padding: 20px;
      border: 1px solid $white;
      border-radius: 4px;
      resize: vertical;
      margin-bottom: 20px;
    }

    label{
      padding: 12px 12px 12px 0;
      display: inline-block;
    }

    input[type=submit]{
      background-color: $grey;
      color: white;
      padding: 12px 20px;
      margin-top: 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: left;
    }

  }

  nav {
    position: relative;
    color: $white;
    background-color: $darkblue;
    padding: 8px 24px;
    h1 {
      color: $white;
    }
    span {
      position: absolute;
      left: 16px;
      top: 12px;
      a {
        color: $white;
        text-decoration: none;
      }
    }
  }

  .container-admin {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    padding: 8px 24px;

    a {
      display: flex;
      height: 160px;
      width: 200px;
      background: $white;
      color: $darkblue;
      font-weight: 600;
      text-decoration: none;
      margin: 16px;
      text-align: center;
      border: 3px solid $darkblue;
      justify-content: center;
      align-items: center;
      transition: 0.2s;

      &:visited {
        color: $darkblue;
      }

      &:hover {
        background-color: #dee3e4;
        text-decoration: underline;
      }

      p{
        margin: 0;
      }

    }
  }

  .population {
    width: 100%;
    h2 {
      width: 100%;
      max-width: 720px;
      margin: 32px auto 0;
    }
    .user-add {
      margin: 8px 0 16px;
      form {
        width: 100%;
        max-width: 720px;
        margin: 0 auto;
        display: flex;
        flex-wrap: wrap;
      }
    }
    table {
      border-spacing: 0;
      width: 100%;
      max-width: 720px;
      margin: 16px auto 0;
      flex-direction: row;
      tr:nth-child(odd) {
        background: $lightblue;
      }
      tr:nth-child(even) {
        background: $whiteblue;
      }
      td {
        padding: 8px;
        &:first-child {
          text-align: center;
        }
        button {
          padding: 8px;
        }
      }
    }
  }

  .container {
    display: flex;
    align-items: center;
    flex-flow: column;

    h2{
      margin-top: 42px;
      font-size: 32px;
    }

    img{
      width:250px;
    }

    .col{
      margin-top:45px;

      p{
        max-width: 259.75px;
        padding-bottom: 10px;
      }

    }

    .event-add {
      margin-top: 50px;
      max-width: 600px;
      width: 100%;
      h2 {
        margin: 32px 0 0;
      }

      input,
      button {
        width: 100%;
        margin-bottom: 8px;
      }
    }
  }
}
