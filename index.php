

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="assets/styles/login.css">
    <link rel="stylesheet" type="text/css" href="media_queries.css">
    <link rel="shortcut icon" href="assets/images/VT_logo_small_white.svg" type="image/x-icon">
    <title>Villaticus</title>
</head>

<body>

    <div class="wrapper">

        <div class="div1">

           <div class="div1_in1">

                <div class="logo">
                    <img src="assets/images/VT_logo_white.svg" alt="logo">      
                    <p>Information system for village citizens - <b>simply</b>.</p>                                                           <!-- logo  JE TŘEBA VLOŽIT!!!!!-->
                </div>

                <div class="login">

                    <h2>Log in</h2>

                    <form action="assets/php/login.php" method="post">
                        <input type="text" name="user_username" placeholder="username" required>
                        <br>
                        <input type="password" name="user_pwd" placeholder="password" required>
                        <br>
                        <button type="submit" name="login_submit">Log In</button>
                    </form>

                    <!-- WOF -->

                </div>

                <a href="register.php" class="register">Register me</a>

            </div>

        </div>

        <div class="div2">
            
        </div>

    </div>

</body>

</html>