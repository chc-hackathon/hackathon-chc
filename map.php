<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/styles/main.css">
    <link rel="stylesheet" type="text/css" href="media_queries.css">
    <title>Villaticus</title>
</head>
<body>

<header>
<div class="container">
  <div class="row">
    <div class="col-sm-6 col-6">
    <a href=""><img class="logo_image" src="assets/images/VT_logo_small_BLUE.svg"></a>
    </div>

    <div class="col-sm-6 col-6">
        <p><a id="logout" href="">LOG OUT</a></p>
    </div>
  </div>
</div>
</header>

<div class="name">
<div class="container">
  <div class="row">
    <div class="col-sm-12">
        <h1 class="nadpis">Banov</h1>
    </div>
  </div>
</div>
</div>

<div class="container">

  <iframe src="https://www.google.com/maps/d/embed?mid=1vJvIZxxn3PMQToQ-I6vK_bJWFIKyhXb6" width="640" height="480"></iframe>

</div>
    
</body>
</html>